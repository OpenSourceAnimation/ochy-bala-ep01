# Source files of **Ochy-Bala - Episode 1**.

IMPORTANT: This repository uses Git LFS (https://git-lfs.github.com/). Please make sure to install it before you clone.

To build sources of this animation you will need following software installed:

* OpenToonz >= 1.4
* Blender 2.83.18
* RenderChan (https://morevnaproject.org/renderchan/)

NOTE: All those applications are free software, so you can download and use them for free.

Rendering files:

```
renderchan ~/ochy-bala-ep01/project/project-ep01-ru.blend
```

When rendering is completed you will find resulting file in "render" subdirectory - `~/ochy-bala-ep01/project/render/project-ep01-ru.blend`.

NOTE: It is assumed you unpacked project sources into “~/ochy-bala-ep01/” directory.

If you plan to contribute into this repository, please consider to read important instructions here - https://gitlab.com/OpenSourceAnimation/ochy-bala-ep01/-/blob/master/CONTRIBUTING.md
