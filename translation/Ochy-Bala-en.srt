1
00:00:00,268 --> 00:00:03,928
College of Culture and Arts named after G.I. Choros-Gurkin

2
00:00:04,835 --> 00:00:06,535
in association with «Morevna Project» OATDC and «Adamant» Youth Centre

3
00:00:08,506 --> 00:00:10,326
presents

4
00:00:12,241 --> 00:00:15,681
Wide and beautiful is Altai land.

5
00:00:16,010 --> 00:00:22,360
Under the sheltering skies, at the foot of mountains the Altai people live in happiness and prosperity.

6
00:00:22,703 --> 00:00:26,443
Two sisters rule in these lands.

7
00:00:26,689 --> 00:00:31,039
The elder one, the protectress of peace named Ochira-Mandi

8
00:00:31,313 --> 00:00:36,853
and the younger one, valiant and fearless warrior named Ochy-Bala.

9
00:00:37,829 --> 00:00:41,029
 And this is my story.

10
00:00:42,987 --> 00:00:45,987
 Ochy-Bala

11
00:00:47,605 --> 00:00:51,565
We lived in peace in our sacred land

12
00:00:51,872 --> 00:00:53,632
 when a woe knocked at our door.

13
00:00:56,861 --> 00:01:02,344
The wicked and merciless ruler of the neighbour land Khan-Taadi-Biy

14
00:01:02,344 --> 00:01:04,908
sent his son Ak-Dyaal to declare war.

15
00:01:04,963 --> 00:01:06,629
To plunder sacred Altai,

16
00:01:06,696 --> 00:01:08,568
to trample our settlements,

17
00:01:08,722 --> 00:01:11,242
to enslave the Altai people.

18
00:01:13,173 --> 00:01:17,113
I could not let it happen.

19
00:01:19,161 --> 00:01:23,323
After this battle, I sought advice from my sister.

20
00:01:23,373 --> 00:01:27,739
It was decided that I would travel to other’s lands

21
00:01:27,745 --> 00:01:31,945
to meet Khan-Taadi-Biy and negotiate peace.

22
00:01:33,000 --> 00:01:35,673
Then my old loyal friend,

23
00:01:35,673 --> 00:01:39,013
warrior’s horse Ochy-Deryen came down from the sky

24
00:01:41,425 --> 00:01:46,585
And together we took off like a whirlwind above the lush green hills.

25
00:01:47,167 --> 00:01:50,859
Seven days and seven nights took our flight.

26
00:01:50,859 --> 00:01:54,539
The Khan’s land was almost

27
00:01:56,297 --> 00:02:00,203
there when at border of the red lands I met its mighty guard,

28
00:02:00,203 --> 00:02:02,063
the Blue Bull.

29
00:02:02,608 --> 00:02:05,088
The monster was standing amidst the smoke,

30
00:02:05,165 --> 00:02:07,826
enveloped with flame,

31
00:02:07,826 --> 00:02:09,906
thunder heard inside,

32
00:02:10,533 --> 00:02:15,191
batyrs’ breathless bodies lying around.

33
00:02:15,687 --> 00:02:18,000
Doubt crawled into my soul.

34
00:02:18,000 --> 00:02:23,640
I did not know whether  I could win the battle with such an opponent.

35
00:02:24,397 --> 00:02:27,677
But I looked into my sutra of wisdom

36
00:02:28,958 --> 00:02:32,454
I learnt that the monster had a weakness which was

37
00:02:32,454 --> 00:02:35,574
two veins of steel on its back.

38
00:02:35,750 --> 00:02:39,338
Then my mighty steed turned into a red bull,

39
00:02:39,338 --> 00:02:42,813
and I turned into an eagle of nine wings.

40
00:02:43,080 --> 00:02:45,300
And I soared high into the sky.

41
00:03:01,968 --> 00:03:02,708
At last,

42
00:03:03,246 --> 00:03:06,226
I reached Khan-Tadi-Biy’s land.

43
00:03:07,085 --> 00:03:14,325
The red lands inhabited by his people laid before my eyes.

44
00:03:18,000 --> 00:03:24,820
To be continued…

45
00:03:25,522 --> 00:03:28,682
Director, artist and animator: Anastasiya Mayzhegisheva

46
00:03:29,572 --> 00:03:31,612
Academic Advisor: Sergey Dykov

47
00:03:32,118 --> 00:03:33,678
Script writers: Konstantin Dmitriev, Anastasiya Mayzhegisheva

48
00:03:33,678 --> 00:03:35,318
Voice talents: Ochy-Bala – Ailana Boltokova; Khan-Taadi-Biy – Dobrynya Satin

49
00:03:35,934 --> 00:03:38,354
Music by Dobrynya Satin

50
00:03:39,000 --> 00:03:41,340
Altai translation: Ayaru Chekonova

51
00:03:42,221 --> 00:03:44,221
Special thanks to: Yaroslava Shitova

52
00:03:45,000 --> 00:03:49,136
Gorno-Altaisk, 2021

