#!/bin/bash

# https://stackoverflow.com/questions/59895/how-to-get-the-source-directory-of-a-bash-script-from-within-the-script-itself
SOURCE="${BASH_SOURCE[0]}"
DIR="$( cd -P "$( dirname "$SOURCE" )" >/dev/null 2>&1 && pwd )"
DIR=`dirname "${DIR}"`
cd $DIR
pwd

for FILE in scenes/*.tnz;do
  #echo "$FILE"
  #ls "$FILE"
  renderchan "$FILE"
done

