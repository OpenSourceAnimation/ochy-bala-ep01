<palette id='1'>
  
  <version>
    71 0 
  </version>
  <styles>
    <style>
      color_0 3 255 255 255 0 
    </style>
    <style>
      color_1 4001 "tanda/oil-03-clean.myb"240 247 255 255 0 
    </style>
    <style>
      dospeh 4001 "classic/brush.myb"0 0 0 255 0 
    </style>
    <style>
      shuba 3 58 33 24 255 
    </style>
    <style>
      gubi 4001 "classic/brush.myb"168 54 52 255 0 
    </style>
    <style>
      kozha 3 221 122 82 255 
    </style>
    <style>
      kozha2 3 211 100 66 255 
    </style>
    <style>
      color_7 3 85 87 89 255 
    </style>
    <style>
      color_8 3 58 33 24 255 
    </style>
    <style>
      odezhda 3 112 0 13 255 
    </style>
    <style>
      meh 3 183 177 175 255 
    </style>
    <style>
      color_11 4001 "classic/brush.myb"88 119 196 255 0 
    </style>
    <style>
      volos 3 32 34 45 255 
    </style>
    <style>
      volos2 4001 "classic/brush.myb"92 86 156 255 0 
    </style>
    <style>
      kozha 3 249 188 158 255 
    </style>
    <style>
      gub 3 222 125 103 255 
    </style>
    <style>
      glaz 3 48 45 68 255 
    </style>
    <style>
      color_17 3 68 42 40 255 
    </style>
    <style>
      plat 3 31 84 158 255 
    </style>
    <style>
      uzor1 3 31 84 158 255 
    </style>
    <style>
      uzor2 4001 "classic/brush.myb"241 200 122 255 0 
    </style>
    <style>
      naplechniki 3 134 132 169 255 
    </style>
    <style>
      gospeh 4001 "deevad/liner.myb"176 174 204 255 0 
    </style>
    <style>
      remen 3 98 61 58 255 
    </style>
    <style>
      shtani 3 64 63 77 255 
    </style>
    <style>
      color_25 3 255 219 186 255 
    </style>
    <style>
      color_26 3 159 104 80 255 
    </style>
    <style>
      color_27 3 237 235 245 255 
    </style>
    <style>
      color_28 3 76 88 160 255 
    </style>
    <style>
      color_29 3 100 70 75 255 
    </style>
    <style>
      color_30 3 181 93 122 255 
    </style>
    <style>
      color_31 4001 "classic/brush.myb"222 140 61 255 0 
    </style>
    <style>
      color_32 3 248 188 111 255 
    </style>
    <style>
      color_33 3 187 47 66 255 
    </style>
    <style>
      color_34 3 103 156 234 255 
    </style>
    <style>
      odezhda 4001 "deevad/liner.myb"188 63 102 255 0 
    </style>
    <style>
      kozha2 3 247 159 121 255 
    </style>
    <style>
      teni 4001 "classic/brush.myb"165 58 79 255 0 
    </style>
    <style>
      uzor3 4001 "classic/brush.myb"249 144 38 255 0 
    </style>
    <style>
      remen2 3 124 142 90 255 
    </style>
    <style>
      color_40 4001 "deevad/liner.myb"170 103 66 255 0 
    </style>
    <style>
      color_41 4001 "deevad/brush.myb"130 146 195 255 0 
    </style>
    <style>
      svitok2 3 204 87 8 255 
    </style>
    <style>
      color_43 4001 "deevad/liner.myb"221 31 31 255 0 
    </style>
    <style>
      color_44 4001 "classic/brush.myb"87 90 150 255 0 
    </style>
    <style>
      teni2 4001 "classic/brush.myb"60 51 160 255 0 
    </style>
    <style>
      svitok 3 224 192 175 255 
    </style>
    <style>
      telo 4001 "deevad/liner.myb"226 64 39 255 0 
    </style>
    <style>
      kopita 3 81 41 35 255 
    </style>
    <style>
      volos 4001 "deevad/liner.myb"30 16 43 255 0 
    </style>
    <style>
      color_50 4001 "classic/brush.myb"0 0 0 255 0 
    </style>
    <style>
      sila 3 255 207 161 255 
    </style>
    <style>
      color_52 3 247 201 92 255 
    </style>
    <style>
      color_53 4001 "classic/brush.myb"224 164 164 255 0 
    </style>
    <style>
      sedlo 3 109 59 36 255 
    </style>
    <style>
      nakidka2 3 102 121 133 255 
    </style>
    <style>
      nakidka1 3 120 149 107 255 
    </style>
    <style>
      color_57 3 253 95 149 255 
    </style>
    <style>
      uzor1 3 142 182 213 255 
    </style>
    <style>
      uzor2 3 222 223 213 255 
    </style>
    <style>
      uzor3 3 114 93 30 255 
    </style>
    <style>
      color_61 4001 "deevad/airbrush.myb"234 57 57 255 0 
    </style>
    <style>
      color_62 3 74 89 43 255 
    </style>
    <style>
      color_63 3 86 46 32 255 
    </style>
    <style>
      color_64 3 14 0 28 255 
    </style>
    <style>
      color_65 3 255 41 41 255 
    </style>
    <style>
      color_66 3 207 223 226 255 
    </style>
    <style>
      color_67 3 109 108 106 255 
    </style>
    <style>
      color_68 3 209 132 98 255 
    </style>
    <style>
      color_69 3 52 101 198 255 
    </style>
    <style>
      color_70 4001 "classic/brush.myb"129 170 247 255 0 
    </style>
    <style>
      color_71 4001 "deevad/liner.myb"36 78 150 255 0 
    </style>
    <style>
      color_72 4001 "classic/brush.myb"29 49 86 255 0 
    </style>
    <style>
      color_73 4001 "classic/brush.myb"0 0 0 255 0 
    </style>
    <style>
      color_74 3 255 255 255 255 
    </style>
    <style>
      color_75 3 249 122 72 255 
    </style>
    <style>
      color_76 4001 "deevad/basic_digital_knife.myb"8 15 38 255 0 
    </style>
    </styles>
  <stylepages>
    <page>
      <name>
        "цвета"
      </name>
      <indices>
        0 1 2 3 4 5 6 7 8 9 10 11 41 44 76 
      </indices>
      </page>
    <page>
      <name>
        OCH 
      </name>
      <indices>
        12 13 14 15 16 18 19 20 21 22 23 24 35 36 38 39 17 37 40 42 43 45 46 53 74 
      </indices>
      </page>
    <page>
      <name>
        OM 
      </name>
      <indices>
        25 26 27 28 29 30 31 32 33 34 
      </indices>
      </page>
    <page>
      <name>
        horse 
      </name>
      <indices>
        47 48 49 50 51 52 54 55 56 57 58 59 60 61 75 
      </indices>
      </page>
    <page>
      <name>
        "page 5"
      </name>
      <indices>
        62 63 64 65 66 67 68 69 70 71 72 73 
      </indices>
      </page>
    </stylepages>
  <shortcuts>
    9 0 1 2 3 4 5 6 7 8 
  </shortcuts>
  
</palette>
